import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AuthService } from './auth.service';
import { environment } from '../../environments/environment';

/**
 * A service that provides an API Access to Live offers.
 * 
 * This shows basic usage of typescript, as well as example of 
 * a typical CRUD HTTP service that is used when building an Angular app
 */
@Injectable()
export class ExploreLiveOffersService {
  constructor(private readonly http: HttpClient,
    private readonly authService: AuthService) {
  }

  /**
   * Returns paginated result for live offers
   */
  getSuggestedOffers(page: number, currentBounds?: [number, number]) {
    let url = `${environment.apiUrl}services/live-offer-suggested`;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.authService.token}`
      }),
    };
    return this.http.get(url + `${page > 0 ? '?page=' + page : ''}${currentBounds !== undefined ? '&geoWithin=['+ '[' + currentBounds[0] + ']' + ',' + '[' + currentBounds[1] + ']' + ']' : ''} `, httpOptions);
  }

  /**
   * Get Offer by ID
   */
  getRequestData(id: string) {
    let url = `${environment.apiUrl}request/${id}`;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.authService.token}`
      }),
    };
    return this.http.get(url, httpOptions);
  }

  /**
   * Delete offer
   */
  deleteRequest(id: string) {
    let url = `${environment.apiUrl}request/${id}`;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.authService.token}`
      }),
    };
    return  this.http.delete(url, httpOptions);
  }

  /**
   * Update offer
   */
  updateRequest<T>(id: string, payload:T) {
    let url = `${environment.apiUrl}request/${id}`;
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.authService.token}`
      }),
    };
    return this.http.put(url,payload, httpOptions);
  }
}
